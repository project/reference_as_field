<?php

namespace Drupal\reference_as_fields_formatter\Plugin\Field\FieldFormatter;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\TranslatableInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 *
 * @FieldFormatter(
 *   id = "merge_into_content_formatter",
 *   label = @Translation("Fields in parent content"),
 *   field_types = {
 *     "entity_reference",
 *     "entity_reference_revisions"
 *   }
 * )
 */
class MergeReferenceIntoContentFormatter extends EntityReferenceFormatterBase
{


  protected EntityRepositoryInterface $entityRepository;

  protected EntityTypeManagerInterface $entityTypeManager;

  protected string $targetEntity;


  /**
   * MergeReferenceIntoContentFormatter constructor.
   * @param $plugin_id
   * @param $plugin_definition
   * @param FieldDefinitionInterface $field_definition
   * @param $settings
   * @param $label
   * @param $view_mode
   * @param $third_party_settings
   * @param EntityRepositoryInterface $entity_repository
   * @param EntityTypeManagerInterface $entity_typemanager
   */
  public function __construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings, $entity_repository, $entity_typemanager)
  {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityRepository = $entity_repository;
    $this->entityTypeManager = $entity_typemanager;
  }


  public function settingsForm(array $form, FormStateInterface $form_state)
  {
    $view_modes = $this->getConfigurableViewModes();
    if (!empty($view_modes)) {
      $form['view_mode'] = [
        '#title' => t('View Mode'),
        '#description' => t('Select the view mode which will control which fields are shown and the display settings of those fields.'),
        '#type' => 'select',
        '#default_value' => $this->getSettings()['view_mode'],
        '#options' => $view_modes,
      ];
    }
    $form['show_entity_label'] = [
      '#title' => t('Display Entity Label'),
      '#description' => t('Should the label of the target entity be visible?'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('show_entity_label'),
    ];

    return $form;
  }

  public function settingsSummary()
  {
    $return = ['#markup' => $this->t('Fields shown in "@view_mode" view-mode.', ['@view_mode' => $this->getSetting('view_mode')])];
    if ($this->getSetting('show_entity_label')) {
      $return['#markup'] .= ' ' . $this->t('with a label');
    }
    return $return;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode)
  {
    $elements = [];
    if ($items && $entities = $this->getEntitiesForViewing($items, $langcode)) {
      $fieldDefinition = $items[0]->getFieldDefinition();
      $targetBundleFieldDefinition = $this->getTargetBundleId($fieldDefinition);

      $renderArray = $this->getRenderArray($this->targetEntity, $targetBundleFieldDefinition, $entities);
      $elements += $renderArray;

      $this->buildCacheMetadata($entities)->applyTo($elements);
    }
    return $elements;
  }


  /**
   * Get the renderer for the given entity type and/or bundle
   * @param $type
   * @param $bundle
   * @return mixed
   */
  public function getRendererForDisplay($type, $bundle)
  {
    $bundle = $bundle ?: $type;
    $storage = $this->entityTypeManager->getStorage('entity_view_display');
    $renderer = $storage->load(implode('.', [$type, $bundle, $this->getSetting('view_mode')]));
    if (!$renderer) {
      $renderer = $storage->load(implode('.', [$type, $bundle, 'default']));
    }
    return $renderer;
  }


  /**
   * @param FieldItemListInterface $items
   * @param string $langcode
   * @return array
   */
  public function getEntitiesForViewing($items, $langcode): array
  {
    $this->targetEntity = $this->getTargetEntityId($this->fieldDefinition);

    $entity_storage = $this->entityTypeManager->getStorage($this->targetEntity);
    $entities = [];
    foreach ($items as $item) {

      $itemID = $this->getEntityIdFromFieldItem($item);
      $entity = $entity_storage->load($itemID);
      if ($entity !== NULL && $entity->access('view')) {
        if ($entity instanceof TranslatableInterface) {
          $entity = $this->entityRepository->getTranslationFromContext($entity, $langcode);
        }
        $entities[] = $entity;
      }
    }
    return $entities;
  }


  /**
   * Build the render array for the given entities.
   *
   * @param $type
   * @param $bundle
   * @param $entities
   *
   * @return array
   * @throws PluginNotFoundException
   */
  public function getRenderArray($type, $bundle, $entities): array
  {
    $displayRenderer = $this->getRendererForDisplay($type, $bundle);
    $renderCandidates = $displayRenderer->buildMultiple($entities);
    $renderCandidates = reset($renderCandidates);
    $renderCandidates = array_filter($renderCandidates, [
      $this,
      'fieldHasRenderableContent',
    ]);

    if (!$this->getSetting('show_entity_label')) {
      $labelField = $this->entityTypeManager->getDefinition($type)->getKey('label');
      unset($renderCandidates[$labelField]);
    }

    uasort($renderCandidates, [SortArray::class, 'sortByWeightProperty',]);
    return $renderCandidates;
  }

  /**
   * @param array $entities
   *
   * @return CacheableMetadata
   */
  public function buildCacheMetadata($entities)
  {
    $cache_metadata = new CacheableMetadata();
    foreach ($entities as $entity) {
      // Todo check if the entity is needed for viewing so the cache tags make some sense
      $cache_metadata->addCacheableDependency($entity);
      $cache_metadata->addCacheableDependency($entity->access('view', NULL, TRUE));
    }
    return $cache_metadata;

  }

  /**
   * @param FieldDefinitionInterface $field_definition
   *
   * @return array|mixed
   * @throws \Exception
   */
  protected function getTargetBundleId(FieldDefinitionInterface $field_definition)
  {
    $fieldDefinitionSettings = $field_definition->getSettings();
    if (strpos($fieldDefinitionSettings['handler'], 'default') === 0) {
      // Default to the first bundle, currently only supporting a single bundle.
      $target_bundle = array_values($fieldDefinitionSettings['handler_settings']['target_bundles']);
      $target_bundle = array_shift($target_bundle);
    } else {
      throw new \Exception('Using non-default reference handlers are not supported');
    }
    return $target_bundle;
  }


  /**
   * Check if the field can be rendered.
   *
   * @param $field
   *
   * @return bool
   */
  protected function fieldHasRenderableContent($field)
  {
    return (isset($field['#formatter'], $field['#items']) && $field['#items']->getFieldDefinition()
        ->getDisplayOptions('view'));
  }

  /**
   * @return array
   */
  protected function getConfigurableViewModes()
  {
    $entityDisplayRepository = \Drupal::getContainer()->get('entity_display.repository');
    return $entityDisplayRepository->getViewModeOptions($this->getTargetEntityId($this->fieldDefinition));
  }

  /**
   * @param FieldDefinitionInterface $field_definition
   *
   * @return mixed
   */
  protected function getTargetEntityId(FieldDefinitionInterface $field_definition)
  {
    return $field_definition->getFieldStorageDefinition()
      ->getSetting('target_type');
  }

  /**
   * @param FieldItemInterface $item
   *
   * @return mixed
   */
  protected function getEntityIdFromFieldItem(FieldItemInterface $item)
  {
    return $item->getValue()['target_id'];
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity.repository'),
      $container->get('entity_type.manager')
    );
  }


  public static function defaultSettings()
  {
    return [
      'view_mode' => 'default',
      'show_entity_label' => 0,
    ];
  }
}
